var express = require('express');
var app = express();

var empregados = [
  {
    nome: "Thiago",
    salario: 123
  },

  {
    nome: "Paulo",
    salario: 123
  },
];

app.get('/api/empregados', function (req, res) {
  res.send(empregados);
});

app.listen(3000, function () {
  console.log('Server executando em 0.0.0.0:3000')
});
