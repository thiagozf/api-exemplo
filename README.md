# Promises

Exemplo para uso de promises (JS)

----------

#### Instalar dependências

```
npm install
```


#### Executar projeto

```
npm start
```


#### Rodar testes

```
npm test
```


#### Distribuir app

```
npm build
```
